# Class Assignment 5 Report - Part 1 - Practice with Jenkins

__Installing Jenkins and preparing repository with chat app__

- I started by copying my basic gradle project (already on ca2/part1) to ca5/Part1 file.
- To be able to work with Jenkins i had to download the war file from jenkins site, and put it in ca5 file:

		 https://jenkins.io/download/
    	 The link used was "Generic Java Package(.war)"
- To run Jenkins service, we have to use the next command:

	 	java -jar jenkins.war
- During the process, it is indicated that for initial setup a password is required. A generated one is displayed and can also be found in the file __"C:\Users\Alexandre\.jenkins\secrets\initialAdminPassword"__. To access the initial setup i had to open a browser on __"localhost:8080"__ and insert the password to unlock jenkins.

- To customize Jenkins i chose __"Install suggested plugins"__ and waited for the configuration to finish installing some predefined plugins. These can be added or removed later.

- After starting Jenkins GUI, we can now make all configurations to users, plugins and projects as well as checking system information and configuration.

- The new job we are going to create is going to retrieve my project from my repository, and for that i have to create the jenkinsfile and put it inside my repositroy through git. I won't make the repository private because it would ruin my other class assignments that fetched my public repository.

____
__Create jenkinsfile__

- We can now start working on our jenkinsfile. This is were, through Groovy code, we tell jenkins what it is going to execute between stages and what order those will run. This file is located in the root folder of the source code repository.

- The actual file was obtained from __"https://gist.github.com/atb/2a5587bd5392dfada81a2f74e811824b"__ but with some changes to meet with the requirements:

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops-19-20-b-1060237.git'
            }
        }
        stage('Assemble') {
            steps {
                dir('ca5/Part1/basic') {
                    echo 'Assembling...'
                    bat 'gradlew clean assemble'
                }
            }
        }
        stage('Test') {
            steps{
                dir('ca5/Part1/basic') {
                    echo 'Testing...'
                    bat 'gradlew test'
                    junit 'build/test-results/test/*.xml'
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca5/Part1/basic/build/distributions/*'
            }
        }
    }
}
```
- As we can see, i added my repository link to __"Checkout"__ stage, where jenkins gets all files and in my case puts them in __"C:\Users\Alexandre\.jenkins\workspace\ca5-part1-1060237"__

- In Assemble stage, a step "dir" was added to inform jenkins to do the next command inside that folder. "gradlew clean assemble" was nedded because we want the tests to be done afterwards and "clean" so every time this pipeline is built the tests folder is removed and built again. This prevents an error in the next  test stage (if the test files are more than 300ms older the stage fails).
- The Test stage was added to run __"gradlew test"__ and __"junit 'build/test-results/test/*.xml'"__ is used to archive junit-formatted test results.

- The Archiving stage was updated with the right directory location (__"archiveArtifacts 'ca5/Part1/basic/build/distributions/*'"__)
___
__Update my bitbucket repository__
	
- To have my repository ready for Jenkins i added an issue, commited and pushed my changes:
		
		git add ca5/
		git commit -m "Ref #28 - Adds chat app project to ca5/part1/basic with jenkinsfile"
		git push
____
__Create new job__

- Being logged on to Jenkins at localhost:8080, i selected new item. There i selected pipeline job and entered __"ca5-part1-1060237"__ as item name.
- In job configuration, i left every checkbox empty, but on "Pipeline" i selected:
		
		Definition: Pipeline script from SCM
		SCM: Git
		Repository URL: https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops-19-20-b-1060237.git
		Branch Specifier:*/master
		Script Path: ca5/Part1/basic/jenkinsfile
	
- This configuration allows Jenkins to retrieve my project and use my remote Jenkinsfile. Now it is possible to build the created pipeline and get all the process feedback.

- Tagged my repository with git command:

		git tag ca5-part1
---

## Part2 - tutorial spring boot application pipeline

- The first step was to put the application in question inside ca5/part2 file, along with the jenkinsfile and commit.

- I made some changes to jenkinsfile to adapt it to work with the spring boot application, now on ca5/part2 folder and added the __"Javadoc"__ stage (for this i had to add the htmlpublisher plugin on jenkins):

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops-19-20-b-1060237.git'
            }
        }
        stage('Assemble') {
            steps {
                dir('ca5/Part2/SpringProject') {
                    echo 'Assembling...'
                    bat 'gradlew clean assemble'
                }
            }
        }
        stage('Test') {
            steps{
                dir('ca5/Part2/SpringProject') {
                    echo 'Testing...'
                    bat 'gradlew test'
                    junit 'build/test-results/test/*.xml'
                }
            }
        }
        stage('Javadoc') {
            steps {
                dir('ca5/Part2/SpringProject') {
                    echo 'Generating javadoc...'
                    bat 'gradlew javadoc'
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca5/Part2/SpringProject/build/libs/*'
            }
        }
    }
}
```



- I tried building the new pipeline with the script manualy inserted to be easier to test it with every try and i had this error:

		No test report files were found. Configuration error?
- Apparently this project did have unitary tests included, but they were not in "src/test" folder but in "src/main" instead. Because of this i edited the project and moved the tests to the correct location. After that i commited and pushed the altered files.

- After that i had problems with distribution folder not existing after assembling. I checked the build file and "distributions" file was missing so i used __"basic/libs/*"__ instead, because its were the "war" file is located.

- At this point the pipeline is built with no problems. Only the new __"Publish Image"__ stage is missing. So to add that stage i had to get the web dockerfile from last assignment and pushed it to my repository. Here are it's contents:

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops-19-20-b-1060237.git

WORKDIR devops-19-20-b-1060237/ca5/Part2/SpringProject

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080

```

- I had to update my docker cache with the last updates to my repository, because if not, when running dockerfile, ca5/Part2 would not even exist.

From now on every time my pipeline is built, every stage comes out green i think i got all requesites achieved!

- To push the created container to docker.hub, i changed the last line on the Publish Image stage with my repository information:

		 docker.build("1060237/switch_devops:${env.BUILD_ID}").push()

- I built again the pipeline, and now at the end it takes more time to send the image but it completed successfully.

- Now i commited changes and tagged my project with:

		git tag ca5-part2