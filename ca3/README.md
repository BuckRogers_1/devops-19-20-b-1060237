# Class Assignment 3 Report

I started by downloading and installing the hypervisor __Virtual Box__.

The Ubuntu minimal installation iso was mounted and the machine started so it could be installed.

Two virtual network adapters were configured, one as NAT and the other as Host-only adapter with the next configuration (01-netcfg.yaml file, ater installing network tools):

---
	network:
	 version: 2
	 renderer: networkd 
	 ethernets: enp0s3: 
		dhcp4: yes 
		enp0s8: addresses: - 192.168.56.5/24
---

Next, now in the virtual machine, I installed ssh, vsftpd, git, and java:

---
	- sudo apt install openssh-server
	- sudo apt install vsftpd
	- sudo apt install git
	- sudo apt install openfdk-8-fdk-headless
---

After that i cloned my devops repository to this machine:

---
	- git clone <url>
---

I installed maven because the basic project is a maven project:

---
	- sudo apt install maven
---

I Copied the basic project on Maven from CA1 that uses spring boot and provides a web server with front end. Moved to working directory CA3 part1 and ran the server with:

---
	- mvn spring-boot:run
---

On a browser my real machine, it was possible to access the web server of the virtual machine:

---
	- 192.168.56.5:8080
---

Now, i used the project from class assignment 2 on Gradle to run the chat app. For that i installed Gradle on the "vm":

---
	- sudo apt install gradle
---

The runServer task could not run, because the directories used in the tasks from the last assignment were absolute, so after changing them with nano to relative to the project the problem was solved('.\\src').

Knowing that the client cannot run in the "vm", because there is no front-end, i had to change the project on my PC (win10) by altering the address passed to the runClient task on build.gradle.

---
    task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "
    classpath = sourceSets.main.runtimeClasspath
    main = 'basic_demo.ChatClientApp'
    args '192.168.56.5', '59001'}
---

built the project again:

---
	gradle build
---

Now i can run the client twice on my machine and the server on the "vm". And it worked:

---
	gradle runClient (x2)
---


__Part2__


- Started by creating a folder in the VM(__mkdir ca3/Part2__) and copied my project from CA2-Part 2 to this folder.

- Cloned the Vagrant project supplied. It contains the readme file with the demonstration that two different VM will run, the webserver and the database server. VagrantFile has the configuration so this can happen.

- Installed Vagrant on my machine and moved "vagrantfile" to my repository.

- Changes have to be made to the existing project:


  - So it can support building the war file. This enables its deployment on tomcat. For this, the next lines had to be included in build.gradle:
	
		
			id 'war' (in plugins)
			providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat' (dependency)
		

  - And the class ServletInitializer had to be added to the folder "src/main/java/com.gregIturnquist.payroll":


			package com.greglturnquist.payroll;

       			import org.springframework.boot.builder.SpringApplicationBuilder;
			import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

			public class ServletInitializer extends SpringBootServletInitializer {

			@Override
			protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
			return application.sources(ReactAndSpringDataRestApplication.class);
			    }
			}
		

  - The frontend plugin used in gradle is already with the last version. No changes to build.gradle file.

  - To add support for the database console of h2, some lines had to be added to "src/main/resouces/application.properties":

			spring.data.rest.base-path=/api  
			spring.datasource.url=jdbc:h2:mem:jpadb  
			spring.datasource.driverClassName=org.h2.Driver  
			spring.datasource.username=sa  
			spring.datasource.password=  
			spring.jpa.database-platform=org.hibernate.dialect.H2Dialect  
			spring.h2.console.enabled=true  
			spring.h2.console.path=/h2-console
  - To access h2 through a browser, the next line is added to application.properties too:
			  
			spring.h2.console.settings.web-allow-others=true

  - Now for the application context path to be defined, the next lines must be added to application.properties and "src/main/js/app.js" respectively:

			/basic-0.0.1-SNAPSHOT before "/api/employees" in componentDidMount().
			server.servlet.context-path=/basic-0.0.1-SNAPSHOT to application.properties

  - Again in application.properties, the next lines are added to configure settings so h2 has information persistance on its databases:
  
			spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE 
			(spring.datasource.url=jdbc:h2:mem:jpadb is removed)

  - To ensure that spring does not drop the database on every execution:

			spring.jpa.hibernate.ddl-auto=update

  - With that many changes to application.properties, here is the complete file:

			server.servlet.context-path=/basic-0.0.1-SNAPSHOT  
			spring.data.rest.base-path=/api  
			spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE  
			spring.datasource.driverClassName=org.h2.Driver  
			spring.datasource.username=sa  
			spring.datasource.password=  
			spring.jpa.database-platform=org.hibernate.dialect.H2Dialect  
			spring.jpa.hibernate.ddl-auto=update  
			spring.h2.console.enabled=true  
			spring.h2.console.path=/h2-console  
			spring.h2.console.settings.web-allow-others=true

- Now i had to push all the commits, so when i try to run the two virtual machines, the repository is cloned with all changes.

- the direction to my repository has to be inserted in vagrantfile, and then the commands so it can get to the actual project:

		git clone https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops-19-20-b-1060237.git
      cd devops-19-20-b-1060237
      cd ca3
      cd Part2
      cd SpringProject

- Now i can try to create and run the two VM's with:

			__vagrant up__

  - On the first time vagrant will do the provisioning and install all dependencies included on vagrantfile. 

- A problem ocurred because of permissions to gradlew file, so i added __chmod +x gradlew__ before ./gradlew clean build. After that another error came when the project was being built. So i had to make changes to my project and push those with git push. Now, i added __git pull__ after __cd devops-19-20-b-1060237__, so de VM gets the last changes when provision is run.
  
- With other problems on project compilation, and difficulties to complete web VM provisioning, i connected to the web VM with ssh:

			ssh localhost -P 2200
  - After that, using __vagrant__ as user and password, i had access to de VM but after making changes, vagrant could no longer run the "web VM" because the ssh private key was not working to get the needed access.
 
 - After some tries creating the two VM's from scratch with __vagrant up__, i got it to work. The needed changes were these:
 
   - Task: war - this task was being skipped, so i added to __build.gradle file__:
   			  
			war {
		    enabled = true
			}
   - The line __"sudo cp build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps"__ in vagrantfile was not working. After some digging i found out that __"basic"__ comes from the project name which in my case was __"demo"__. To fix this i changed the line in settings.gradle file to:
   
			   rootProject.name = 'basic'
- Finaly i was able to access the webserver through a browser using:

		http://localhost:8080/basic-0.0.1-SNAPSHOT/

  - And the h2 console through:

		http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console

- Connecting to the database session, i could now add new entries using for example:

			insert into employees values (2, 'hello world', 'alex', 'castro')

---
Just like that, my host machine had a database server and a web server accessing it. Both virtualized!