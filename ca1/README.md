# Class Assignment 1 Report

## 1. Analysis, Design and Implementation

- Comecei por criar um novo repositório na minha conta de bitbucket e selecionei uma pasta no meu disco para armazenar o projeto.
- Na linha de comandos, estando localizado na pasta, foi utilizado o comando __"git clone <URL do meu repositório de bitbucket>"__. Agora, o arquivo .git é criado e
o repositório remoto fica definido.
- Depois de copiar os arquivos e a estrutura de pastas do repositório referenciado pelo professor, o comando __"git add ."__ foi usado para que todos os novos
os arquivos foram rastreados e prontos para o primeiro commit.
- __"git status"__ mostra todas as diferenças do projeto do disco para o projeto no repositório.
- O uso de __"git commit -m <mensagem>"__ seguido de __"git push"__ fez com que o projeto fosse sincronizado.
- __"git tag v1.2.0"__ rotula o último commit na ramificação principal como __"v1.2.0"__
- Para criar uma nova ramificação chamada email-field, foi utilizado o comando __"git branch email-field"__. Para trabalhar nesse novo ramo __"git checkout email-field"__.
- Abri o meu projeto com o IDE e procedi para fazer as alterações necessárias no código.
- Todo o conteúdo da pasta intellij foi adicionado ao .gitignore.
- __"git add ca1/."__ para adicionar diferenças nos arquivos employee.java e app.js. Em seguida, __"git commit -m Fix #4 Class employee - Adicionar campo de email"__. Fix#4 resolve a issue criada no bitbucket.
- Caso haja necessidade de voltar ao estado de um commit anterior podemos usar __"git reset [commit]"__. Caso tambem queiramos apagar todas as alteracoes locais posteriores a esse commit usamos "__git reset --hard [commit]"__.
- __"git push"__ gera um erro porque não há informações sobre o que fazer quando há apenas um branch no repositório remoto. Para resolver isso podemos usar __"git push --set-upstream origin email-field"__ ou __"git push -u origin email-field"__para informar ao git para adicionar nova ramificação ao repositório remoto e enviar as alterações sempre para essa nova ramificação.
- Novamente no IDE foi adicionado novo teste com validação de entrada, confirmação e envio.
- __"mvnw spring-boot:run"__ foi usado para executar o servidor e __"localhost: 8080"__ num navegador para aceder à pagina.
- Visto que as alterações feitas no branch email-field estão testadas e a funcionar, fazermos o merge com o master. Para isso fazemos __"git checkout master"__(para trabalhar em 'master'), __"git merge email-field"__ e __"git branch -d email-field"__ para remover o branch.
- Rotulei este ramo com o último commit com "v1.3.0" (__git tag v1.3.0__).

- Para proceder a criacao de um novo branch para resolver bugs foi usado "__git branch fix-valid-email__".
- De seguida procedi à adição da nova funcionalidade(validação do email) e fiz commit das alterações do branch fix-valid-email com __"git add src/."__ e __"git commit -m "implemented email validation""__.
- Para fazer o push foi necessário indicar ao repositório remoto que o branch que vai receber este commit é fix-invalid-email: __"git push -u origin fix-invalid-email"__.
- Com tudo testado e verificado o funcionamento com as alterações, fiz o merge depois de alterar o brach atual para master:__"git checkout master"__ e __"git merge fix-invalid-email"__. __”git tag v1.3.1”__para rotular este patch.
- Podemos agora apagar o branch com __"git branch -d fix-invalid-email"__.

- Para finalizar foi colocada nova tag ao repositorio: __"git tag ca1"__.


## 2. Analysis of an Alternative
-
__Algumas particularidades do darcs, que e também uma dvcs como git.__

- Em vez de commits usa patches que não guardam snapshots como o git.
- Os repositórios são constituídos por sequencias de patches.
- Tem menos comandos sendo que a maioria são interativos, ou seja, vai perguntando o que fazer.
- Não usa branches.
- No que toca ao merge, e ao contrario de git, Darcs transforma os patches de forma a que seja construída uma historia linear.
- Através de Darcs o pull de patches pode ser feito interactivamente e selecionar aqueles que se pretende não tendo em conta a ordem cronológica. Dependências sao obtidas automaticamente.
- E possível, em vez de trabalhar em branches diferentes, criar novos patches com ticket numbers sugestivos que definem uma ordem de implementação.
- Usa uma estratégia matemática complexa para resolver problemas relacionados com dependências através de “Inverses”, “Commutation” e claro, merge.

- Pros:
   - Mais user friendly com menos comandos para o utilizador.
   - Requer menos memória no repositório.

- Cons:
   - Documentação pouco extensa.
   - Pode trazer problemas relacionadas com performance(algoritmo de merge).
   - Menos suporte.
   - Problemas relacionados com compatibilidade no o meio de comunicação para um repositório remoto.


## 3. Implementation of the Alternative

- Criei um repositório em __hub.darcs.net__. Foi-me dada a informação de qual o comando para fazer push: __push 1060237@hub.darcs.net:devops-19-20-B-1060237__criei a pasta CA1_Alternativo.
- Abri a linha de comandos e fui para o diretório do repositório local.
- __"darcs init"__ para começar o controlo de versões do repositório.
- Copiei o conteúdo do projeto basic para a pasta atual.
- Usei o comando __"darcs record -l"__ para adicionar o projeto inicial com todas as pastas e ficheiros incluídos. O programa ira perguntar pelo email pessoal para ser usado para saber o autor dos patches, tal como o nome deste patch.
- Para cada ficheiro é feita a pergunta se queremos adicionar. __"a"__ serve para adicionar todos.
- Inseri o comando __darcs show files__ e revelou a lista de ficheiros do projeto já a serem controlados pelo vcs.(git status)
- Adicionei uma tag que representa a versão inicial sem alterações (__darcs tag v1.2.0__).
- Para fazer o push usei __"darcs push 1060237@hub.darcs.net:devops-19-20-B-1060237"__ mas houve um erro de autenticação ao servidor.
- Acontece que para utilizadores de windows podem haver questões relacionadas com o acesso através de ssh, nomeadamente o uso de uma chave publica e processos de encriptacao obsoletos que obrigam a mais configurações. Estas questões advêm do facto de ser uma ferramenta com poucos windows users e pouco suporte.
- Em git criamos um novo branch mas em darcs normalmente existem duas arvores, uma "working" outra "pristine". "Working tree" são todos os ficheiros no repositório, "pristine tree" representa o registo de alterações.
- Portanto em analogia ao git quando trabalhamos sem fazer record(merge), e como estar a trabalhar num branch sem ser o master. No exemplo da alteração email-field, apos fazer as alterações e testar, usaríamos push e depois de selecionar o pretendido usaríamos record"

- Alguns dos comandos deste dvcs:
   -init     -add      -revert
   -clone    -move     -rollback
   -pull     -whatsnew  -send
   -push     -diff     -rebase
   -log      -record       
   -add      -amend    
