# __CA4__
 

## As reference, here are some commands available to execute with docker:

			
- Docker information:


			docker info
					
- List local docker images: 


			docker images

- Start a docker container:


			docker run [image] 

- List running containers: 


			docker ps

- Run a command on the container: 


			docker exec -it [container ID] [command]

- Stop a running container: 


			docker stop [container ID] 

- Remove a container:


			docker rm [container ID] 

- Remove an image: 


			docker rmi [image] 

- Usage:


			sudo docker run --name [container name] -i -t [image] [command] [arguments] 

- Change container state:


			docker pause/unpause/start/restart/stop/run/kill [container id]

- Create new image from running container:


			docker commit [container ID] [image name]

- Push images to socker hub:


			docker login
			docker push [image]:[tag]

- Create image from Dockerfile:


			docker build .
			docker build -f [path-to-dockerfile] -t [tag] .

## And here are some commands that can be used to define and run multi-container applications (Docker Compose):

- Get all containers running:

			docker-compose up

- Build the images:

			docker-compose ps

- Execute a command inside the container:

			docker-compose exec <container name> <command name>

- Make a list of all containers:

			docker-compose ps

- Start or stop any container:

			docker-compose start/stop <container name>

- Remove any stopped container:

			docker-compose rm <container name>

- List images:

			docker-compose images

## Here is a description of what some instructions mean inside each "dockerfile" and the docker-compose file:


- __Docker-compose:__   


		Build container based on image - __"build:web"__
	
		Port mapping to use on each side - __ports:"8080:8080"__
		
		Ip selection to use on container - __"ipv4_address: 192.168.33.10"__
		
		Specifies dependency (database runs first) - __"depends_on - "db""__
		
		Enables file sharing between host / container(separated by ":") - __"volumes: - ./data:/usr/src/data"__
	
---------------------------------------		

- __web dockerfile:__


		Starts with existing tomcat image - __"FROM tomcat"__

		Installs all nedded software so the application can be built - __"RUN apt-get install <git/nodejs/npm>"__

		Creates temporary folder where the application files will be sent - __"RUN mkdir -p /tmp/build"__

		Changes work directory to the created directory - __"WORKDIR /tmp/build/"__

		Clones the referenced repository into the working directory - __"RUN git clone https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops-19-20-b-1060237.git"__

		Changes working directory to where the application is - __"WORKDIR devops-19-20-b-1060237/ca4/SpringProject"__

		Gives execution permission to gradlew - __"RUN chmod +x gradlew"__

		Builds the spring web application - __"RUN ./gradlew clean build"__

		Copies .war file to be used by tomcat - __"RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/"__

		Tomcat will be accessible on port 8080 - __"EXPOSE 8080"__

		No other command is done so tomcat continues running the war application.
	
---------------------------------------

- __db dockerfile:__


		Starts with existing ubuntu image - __"FROM ubuntu"__

		Installs all nedded software so the application can be built - __"RUN apt-get install <java/unzip/wget>"__
	
		Creates "app" folder where h2 will be installed - __"RUN mkdir -p /usr/src/app"__

		Changes work directory to the created directory - __"WORKDIR /usr/src/app"__

		Gets "h2" application jar from maven repository- __"RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar"__

		"h2" will be accessible on port 8082 and 9092 - __"EXPOSE 8082"__/__"EXPOSE 9092"__

		Runs org.h2.tools.Server class on "h2" with some options. This has to be done because this image is not dedicated to "h2". - __"CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists"__
        

## With that out of the way, here is a detailed description of the steps needed to put the database and web application dockers running...

- Cloned the repository from  https://bitbucket.org/atb/docker-compose-spring-tut-demo.git with web and db docker configurations and copied it to my ca4/docker folder.


- Some changes were made to the web application docker file:

	
	- Changed Workdir command to __WORKDIR devops-19-20-b-1060237/ca4/SpringProject__, so the next commands are done where the web application is located.
	- Added __RUN chmod +x gradlew__ before "./gradlew clean build". This way permission is set so it is able to be executed.
	- Changed the war file to its proper name -> __RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/__

- In relation to docker-compose file, nothing had to be changed, but in my case, docker desktop was used, and the drive were i had the docker files had to be added to the mountable drives  on "settings/resources/file sharing".

- To build the two images, i changed the working directory to \devops-19-20-b-1060237\ca4\docker\ and used:

			docker-compose build

- I had some problems building the web application along the way and after finding the errors i tried several times, so ultimately i had to clean all containers, volumes etc, including cashe files. For that i used:

			docker system prune -a

- After getting a complete build, to execute and then know the state of the containers, i used:

			docker-compose up
			docker-compose ps

- At this point both containers are up. Using a browser i accessed the web application and, as expected, got the first entry(frodo) with:

			http://localhost:8080/demo-0.0.1-SNAPSHOT/

- Next i tried the h2 console, added new entries and tried again on the browser with the application, and there where the new entries on the web app table. For this i used:

			http://localhost:8082
			(login)JDBC URL: jdbc:h2:tcp://192.168.33.11:9092/./jpadb  

- To make sure there is persistence, i stopped and started both containers, and after that i checked if the entries where still there:

			docker-compose stop
			docker-compose start

- If i want to backup the database information i have to access the db container and execute bash with:

			docker-compose exec db /bin/bash

- with "ls" verified that the database file was located at /usr/src/app:

			root@cda898389da8:/usr/src/app# ls
			h2-1.4.200.jar  jpadb.mv.db

- Then, copied "jpadb.mv.db" to the shared volume "data""

			cp jpadb.mv.db /usr/src/data/

- Now, in my windows data folder i can find the database file backup.

- With every thing working as expected, i changed the working directory to the repository base folder and commited my ca4 folder and tagged it with ca4:

			git tag ca4

- Now, with the objective of publishing the images to Docker Hub, i accessed the site, and created an account with a repository(named switch_devops):

			https://hub.docker.com/

- Again, on a command line, located at the docker folder, i checked the images names with:

			docker-compose images

- To be able to push the images, i had to change each image's tag with my repository info icluded(if not, an error would occur):

			docker tag docker_web 1060237/switch_devops:ca4_web
			docker tag docker_db 1060237/switch_devops:ca4_db
		

- After that, i logged in to my new hub.docker account with:

			docker login
- Loggin succeeded was shown and now, to push the images to the repository i used:


			docker push 1060237/switch_devops:ca4_web
			docker push 1060237/switch_devops:ca4_db
		
- The upload might take some time, but after that the repository was now populated with these images.